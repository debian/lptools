Source: lptools
Section: python
Priority: optional
Build-Depends: python3-all, dh-python, debhelper-compat (= 12)
Maintainer: Nathan Handler <nhandler@debian.org>
Uploaders: Jelmer Vernooĳ <jelmer@debian.org>
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/debian/lptools.git
Vcs-Browser: https://salsa.debian.org/debian/lptools
Homepage: https://launchpad.net/lptools
Rules-Requires-Root: no

Package: lptools
Architecture: all
Breaks: ubuntu-dev-tools (<< 0.129)
Replaces: ubuntu-dev-tools (<< 0.129)
Recommends: python3-chameleon, python3-indicate
Suggests: python3-tdb
Depends: python3 | ipython3,
         python3-breezy,
         python3-launchpadlib,
         python3-xdg,
         ${misc:Depends},
         ${python3:Depends},
         sensible-utils
Description: Tools for working with Launchpad
 LP Tools allow you to work with Launchpad without ever having to deal
 with the web interface. This package provides the following tools:
 .
  - lp-attach - attach a file to a Launchpad bug
  - lp-bug-dupe-properties - find duplicate Launchpad bug reports
  - lp-capture-bug-counts - view summary of number of bugs for a Launchpad
    project
  - lp-check-membership - check if a launchpad user is a member of a group
  - lp-force-branch-mirror - force a new import
  - lp-get-branches - check out all the branches of a team
  - lp-grab-attachments - download all attachments for specified bugs
    or from bugs for a particular project
  - lp-list-bugs - list all bugs for a project
  - lp-milestone2ical - convert milestones on a project into the iCal format
  - lp-milestones - list and manipulate milestones for a project
  - lp-project - create and manage projects
  - lp-project-upload - upload release files
  - lp-recipe-status - show the status of the recipes owned by a particular user
  - lp-remove-team-members - remove members from a team
  - lp-review-list - list reviews for a project
  - lp-review-notifier - desktop notifier about reviews that can be done
  - lp-set-dup - mark duplicate bugs
  - lp-shell - convenient way to launch Python interpreter already logged in
    to launchpad
